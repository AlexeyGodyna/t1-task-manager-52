package ru.t1.godyna.tm.dto.response.user;

import lombok.NoArgsConstructor;
import ru.t1.godyna.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserLockResponse extends AbstractResultResponse {
}
