package ru.t1.godyna.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.dto.request.user.UserRegistryRequest;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-registry";

    @NotNull
    private final String DESCRIPTION = "registry user.";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken(), login, password, email);
        getUserEndpoint().registryUser(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return null;
    }

}
