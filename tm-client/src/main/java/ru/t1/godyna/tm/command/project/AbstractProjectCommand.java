package ru.t1.godyna.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.endpoint.IProjectEndpoint;
import ru.t1.godyna.tm.command.AbstractCommand;
import ru.t1.godyna.tm.enumerated.Role;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
