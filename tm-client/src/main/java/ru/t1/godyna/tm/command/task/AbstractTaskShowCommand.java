package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.model.TaskDTO;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.TaskNotFoundException;

public abstract class AbstractTaskShowCommand extends AbstractTaskCommand {

    public void showTask(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
