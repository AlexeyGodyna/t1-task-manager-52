package ru.t1.godyna.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.godyna.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.godyna.tm.api.service.IConnectionService;
import ru.t1.godyna.tm.api.service.IPropertyService;
import ru.t1.godyna.tm.dto.model.UserDTO;
import ru.t1.godyna.tm.migration.AbstractSchemeTest;
import ru.t1.godyna.tm.repository.dto.UserDtoRepository;
import ru.t1.godyna.tm.service.ConnectionService;
import ru.t1.godyna.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.godyna.tm.constant.UserTestData.USER1;
import static ru.t1.godyna.tm.constant.UserTestData.USER2;

public class UserDtoRepositoryTest extends AbstractSchemeTest {

    @Nullable
    private static EntityManager entityManager;

    @Nullable
    private static IUserDtoRepository userRepository;

    @BeforeClass
    public static void init() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);
    }

    @AfterClass
    public static void connectionClose() {
        entityManager.close();
    }

    @Before
    public void transactionStart() {
        entityManager.getTransaction().begin();
    }

    @After
    public void transactionEnd() {
        entityManager.getTransaction().rollback();
    }

    @Test
    public void add() {
        Assert.assertNull(userRepository.findOneById(USER1.getId()));
        userRepository.add(USER1);
        Assert.assertNotNull(userRepository.findOneById(USER1.getId()));
    }

    @Test
    public void findAll() {
        add();
        Assert.assertTrue(userRepository.findAll().size() > 0);
    }

    @Test
    public void findOneById() {
        Assert.assertNull(userRepository.findOneById(USER1.getId()));
        add();
        @Nullable UserDTO user = userRepository.findOneById(USER1.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER1.getId(), user.getId());
        Assert.assertNull(userRepository.findOneById(USER2.getId()));
    }

    @Test
    public void findByLogin() {
        Assert.assertNull(userRepository.findByLogin(USER1.getLogin()));
        add();
        Assert.assertEquals(USER1.getId(), userRepository.findByLogin(USER1.getLogin()).getId());
        Assert.assertNull(userRepository.findByLogin(USER2.getLogin()));
    }

    @Test
    public void findByEmail() {
        Assert.assertNull(userRepository.findByEmail(USER1.getEmail()));
        add();
        Assert.assertEquals(USER1.getId(), userRepository.findByEmail(USER1.getEmail()).getId());
        Assert.assertNull(userRepository.findByEmail(USER2.getEmail()));
    }

    @Test
    public void remove() {
        add();
        Assert.assertNotNull(userRepository.findOneById(USER1.getId()));
        userRepository.remove(USER1);
        Assert.assertNull(userRepository.findOneById(USER1.getId()));
    }

    @Test
    public void update() {
        add();
        @NotNull UserDTO user = userRepository.findOneById(USER1.getId());
        Assert.assertNotNull(user);
        user.setLogin("UNIT_TEST");
        user.setFirstName("FIRST_NAME");
        user.setLastName("LAST_NAME");
        user.setMiddleName("MIDDLE_NAME");
        user.setEmail("E@MAIL.RU");
        userRepository.update(user);
        @NotNull UserDTO updatedUser = userRepository.findOneById(USER1.getId());
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(user.getLogin(), updatedUser.getLogin());
        Assert.assertEquals(user.getEmail(), updatedUser.getEmail());
        Assert.assertEquals(user.getLastName(), updatedUser.getLastName());
        Assert.assertEquals(user.getFirstName(), updatedUser.getFirstName());
        Assert.assertEquals(user.getMiddleName(), updatedUser.getMiddleName());
    }

}
