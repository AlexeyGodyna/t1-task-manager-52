package ru.t1.godyna.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.godyna.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.godyna.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.godyna.tm.api.service.IConnectionService;
import ru.t1.godyna.tm.api.service.IPropertyService;
import ru.t1.godyna.tm.dto.model.SessionDTO;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.migration.AbstractSchemeTest;
import ru.t1.godyna.tm.repository.dto.SessionDtoRepository;
import ru.t1.godyna.tm.repository.dto.UserDtoRepository;
import ru.t1.godyna.tm.service.ConnectionService;
import ru.t1.godyna.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.godyna.tm.constant.UserTestData.USER1;
import static ru.t1.godyna.tm.constant.UserTestData.USER2;

public class SessionDtoRepositoryTest extends AbstractSchemeTest {

    @Nullable
    private static EntityManager entityManager;

    @Nullable
    private static IUserDtoRepository userRepository;

    @Nullable
    private static ISessionDtoRepository sessionRepository;

    @BeforeClass
    public static void init() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);
        sessionRepository = new SessionDtoRepository(entityManager);
    }

    @AfterClass
    public static void connectionClose() {
        userRepository.remove(USER1);
        entityManager.close();
    }

    @Before
    public void transactionStart() {
        entityManager.getTransaction().begin();
        if (userRepository.findOneById(USER1.getId()) == null) userRepository.add(USER1);
        if (userRepository.findOneById(USER2.getId()) == null) userRepository.add(USER2);
    }

    @After
    public void transactionEnd() {
        entityManager.getTransaction().rollback();
    }

    @Test
    public void add() {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(USER1.getId());
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void update() {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(USER1.getId());
        sessionRepository.add(session);
        @Nullable final SessionDTO findSession = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(sessionRepository.findOneById(session.getId()));
        Assert.assertEquals(session.getId(), findSession.getId());
        Assert.assertNotEquals(findSession.getRole(), Role.ADMIN);
        session.setRole(Role.ADMIN);
        @Nullable final SessionDTO updatedSession = sessionRepository.findOneById(session.getId());
        Assert.assertEquals(updatedSession.getRole(), Role.ADMIN);
    }

    @Test
    public void remove() {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(USER1.getId());
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.findOneById(session.getId()));
        sessionRepository.remove(session);
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void removeByUserId() {
        @NotNull final SessionDTO userSession1 = new SessionDTO();
        userSession1.setUserId(USER1.getId());
        userSession1.setRole(Role.USUAL);
        sessionRepository.add(userSession1);
        @NotNull final SessionDTO userSession2= new SessionDTO();
        userSession2.setUserId(USER2.getId());
        userSession2.setRole(Role.USUAL);
        sessionRepository.add(userSession2);
        Assert.assertNotNull(sessionRepository.findOneById(userSession1.getId()));
        Assert.assertNotNull(sessionRepository.findOneById(userSession2.getId()));
        sessionRepository.removeByUserId(USER1.getId());
        entityManager.flush();
        entityManager.clear();
        Assert.assertNull(sessionRepository.findOneById(userSession1.getId()));
        Assert.assertNotNull(sessionRepository.findOneById(userSession2.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertEquals(0, sessionRepository.findAll().size());
        @NotNull final SessionDTO userSession1 = new SessionDTO();
        userSession1.setUserId(USER1.getId());
        userSession1.setRole(Role.USUAL);
        sessionRepository.add(userSession1);
        @NotNull final SessionDTO userSession2= new SessionDTO();
        userSession2.setUserId(USER2.getId());
        userSession2.setRole(Role.USUAL);
        sessionRepository.add(userSession2);
        Assert.assertNotNull(sessionRepository.findAll());
        Assert.assertNotEquals(0, sessionRepository.findAll().size());
    }

    @Test
    public void findOneById() {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(USER1.getId());
        Assert.assertNull(sessionRepository.findOneById(session.getId()));
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.findOneById(session.getId()));
    }

    @Test
    public void findOneByIdUserId() {
        @NotNull final SessionDTO userSession1 = new SessionDTO();
        userSession1.setUserId(USER1.getId());
        userSession1.setRole(Role.USUAL);
        @NotNull final SessionDTO userSession2= new SessionDTO();
        userSession2.setUserId(USER2.getId());
        userSession2.setRole(Role.USUAL);
        Assert.assertNull(sessionRepository.findOneByIdUserId(USER1.getId(), userSession1.getId()));
        Assert.assertNull(sessionRepository.findOneByIdUserId(USER2.getId(), userSession2.getId()));
        sessionRepository.add(userSession1);
        sessionRepository.add(userSession2);
        Assert.assertNotNull(sessionRepository.findOneByIdUserId(USER1.getId(), userSession1.getId()));
        Assert.assertNull(sessionRepository.findOneByIdUserId(USER2.getId(), userSession1.getId()));
        Assert.assertNull(sessionRepository.findOneByIdUserId(USER1.getId(), userSession2.getId()));
        Assert.assertNotNull(sessionRepository.findOneByIdUserId(USER2.getId(), userSession2.getId()));
    }

}
