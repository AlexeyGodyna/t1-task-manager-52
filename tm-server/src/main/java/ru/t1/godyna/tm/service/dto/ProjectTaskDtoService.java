package ru.t1.godyna.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.godyna.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.godyna.tm.api.service.IConnectionService;
import ru.t1.godyna.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.godyna.tm.dto.model.TaskDTO;
import ru.t1.godyna.tm.exception.entity.ProjectNotFoundException;
import ru.t1.godyna.tm.exception.entity.TaskNotFoundException;
import ru.t1.godyna.tm.exception.field.ProjectIdEmptyException;
import ru.t1.godyna.tm.exception.field.TaskIdEmptyException;
import ru.t1.godyna.tm.exception.field.UserIdEmptyException;
import ru.t1.godyna.tm.repository.dto.ProjectDtoRepository;
import ru.t1.godyna.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;

public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    @Override
    public ITaskDtoRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @NotNull
    @Override
    public IProjectDtoRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository(entityManager);
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository(entityManager);

        try {
            if (!projectRepository.existsByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
            @Nullable final TaskDTO task = taskRepository.findOneByIdUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeTasksByProjectId(projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = getTaskRepository(entityManager);
        @NotNull final IProjectDtoRepository projectRepository = getProjectRepository(entityManager);
        try {
            if (!projectRepository.existsByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskDTO task = taskRepository.findOneByIdUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
