package ru.t1.godyna.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.model.IRepository;
import ru.t1.godyna.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractRepository <M extends AbstractModel> implements IRepository<M> {

    @Nullable
    protected final EntityManager entityManager;

    protected AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    public void set(@NotNull final Collection<M> models) {
        clearAll();
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.remove(model);
    }

}
